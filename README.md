# README #

Playbooks to provision and update an IGB App Store.

## Setup ###

### Configure control node

To start, install what you need to run the playbooks locally to
create and provision a control node that will run in your AWS
account.

* Install ansible, boto, boto3, and python3.
* Clone this repository.
* Copy `example_common.yml` to `common.yml` and `example_secrets.yml` to `secrets.yml`
* Edit the `common.yml` and `secrets.yml` as indicated in the files to provision control node, which runs in AWS.
* Copy certificates files needed to support https on appstore to `files` (see `example_secrets.yml`)
* Run `ansible-playbook control_node.yml` to provision a control node in your AWS account.

Things you need to know about running control node playbook `control_node.yml`:

1. The control node playbook `control_node.yml` creates a new private key you'll need to log into the control node once it is created and provisioned.
2. The public IP address for your local machine will be added to the control node's security group. 
3. The provisioning process creates a private key (a "pem" file) for the control node and saves it in your user's .ssh home directory. The key is named for the `ec2_name` variable defined in `common.yml`. 
4. You'll use the control node to provision and update an app store. App store sites use https. This means you will need an SSL certificate for the site. You can name the site whatever you want, but you need a valid SSL certificate for it. The control node provisioning process copies these to control node during the provisioning process.
5. Also, the `control_node.yml` playbook clones a copy of the appstore-playbooks repository *onto the control node*. 
 
### Configure and provision an app store

* Log in to the control node using the key created when you created and provisioned the control node.
* *On the control node*, copy example_common.yml to common.yml and examples_secrets.yml to secrets.yml. 
* Edit the newly created files `common.yml` and `secrets.yml` as indicated in the files.
* Run playbook setup.yml.

### Questions? ###

Contact:

* Chester Dias cdias1@uncc.edu
* Ann Loraine aloraine@uncc.edu
* Sameer Shanbhag sshanbh1@uncc.edu